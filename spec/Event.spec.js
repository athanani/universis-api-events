import {ExpressDataApplication} from '@themost/express';
import { SchemaLoaderStrategy, DefaultSchemaLoaderStrategy } from '@themost/data'
import { TestUtils } from '@universis/api/dist/server/utils';
const executeInTransaction = TestUtils.executeInTransaction;

describe('Event', () => {

    /**
     * @type {ExpressDataApplication}
     */
    let application;
    // noinspection JSValidateJSDoc
    /**
     * @type {ExpressDataContext}
     */
    let context;

    beforeAll(() => {
        /**
         * @type {import('express').Application}
         */
        const app = require('@universis/api/dist/server/app');
        /**
         * @type {ExpressDataApplication}
         */
        application = app.get(ExpressDataApplication.name);
        const configuration=application.getConfiguration();
        configuration.setSourceAt('settings/schema/loaders', [
            {
                 loaderType: '@universis/events#EventSchemaLoader'
            }
        ]);
        // reload SchemaLoaderStrategy strategy
        configuration.useStrategy(SchemaLoaderStrategy, DefaultSchemaLoaderStrategy);
        application.useModelBuilder();
        // create context
        context = application.createContext();
    });

    it('should validate event service', async () => {
        const Events = context.model('Event');
        expect(Events).toBeTruthy();
    });

    it('should create event', async () => {
        await executeInTransaction(context, async () => {
            /**
             * @type {import('@themost/data').DataModel}
             */
            const Events = context.model('Event');
            const newEvent = {
                startDate: new Date(),
                endDate: new Date(),
                name: 'Test Event'
            }
            await Events.silent().save(newEvent);
            expect(newEvent.id).toBeTruthy();
            const item = await Events.where('id').equal(newEvent.id).silent().getItem();
            expect(item).toBeTruthy();
            expect(item.name).toEqual(newEvent.name);
        });
    });

    it('should delete event', async () => {
        await executeInTransaction(context, async () => {
            /**
             * @type {import('@themost/data').DataModel}
             */
            const Events = context.model('Event');
            const newEvent = {
                startDate: new Date(),
                endDate: new Date(),
                name: 'Test Event'
            }
            await Events.silent().save(newEvent);
            await Events.silent().remove(newEvent);
            const item = await Events.where('id').equal(newEvent.id).silent().getItem();
            expect(item).toBeFalsy();
        });
    });

});