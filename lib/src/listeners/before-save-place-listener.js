import { DataEventArgs } from "@themost/data";
import { promisify } from 'util';
import util from 'util';
import { DataPermissionEventListener, PermissionMask } from '@themost/data';

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state === 2 && event.target.hasOwnProperty('departments')) {
        const { target: { id, departments }, model: {context} } = event;
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const validateAsync = promisify(validator.validate)
            .bind(validator);
        // try to validate if user has permission to edit place departments
        await validateAsync({
            model: context.model('Place'),
            privilege: 'Place/EditDepartments',
            state: PermissionMask.Execute,
            target: null, // any target
            throwError: true
        });

        // don't remove place departments if they are associated with an open event or timetable 
        for (const department of departments) {
            if (department.$state === 4) {
                const associatedEvent = await context.model('Event')
                    .where('location').equal(id)
                    .and('organizer').equal(department.id)
                    .and('eventStatus/alternateName').equal('EventOpened')
                    .getItem();

                if (associatedEvent) throw new Error(
                    util.format(
                            context.__('There are events associated with the place "%s"'),
                            (await event.model.where('id').equal(id).getItem()).name
                        )
                    );

                const associatedTimetableEvent = await context.model('TimetableEvent')
                    .where('availablePlaces/id').equal(id)
                    .and('organizer').equal(department.id)
                    .and('eventStatus/alternateName').equal('EventOpened')
                    .getItem();

                if (associatedTimetableEvent) throw new Error(
                    util.format(
                            context.__('There are timetable events associated with the place "%s"'),
                            (await event.model.where('id').equal(id).getItem()).name
                        )
                    );
            }
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
