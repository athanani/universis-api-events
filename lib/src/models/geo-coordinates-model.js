import {EdmMapping, DataObject} from '@themost/data';

/**
 * @class
 
 * @property {number} latitude
 * @property {number} longitude
 * @property {PostalAddress|any} address
 * @property {*} addressCountry
 * @property {string} postalCode
 * @property {string} elevation
 * @augments {DataObject}
 */
@EdmMapping.entityType('GeoCoordinates')
class GeoCoordinates extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = GeoCoordinates;
